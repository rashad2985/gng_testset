[
    {
        "author": {
            "avatar_url": "https://avatars.githubusercontent.com/u/5437974?v=3",
            "events_url": "https://api.github.com/users/rashad2985/events{/privacy}",
            "followers_url": "https://api.github.com/users/rashad2985/followers",
            "following_url": "https://api.github.com/users/rashad2985/following{/other_user}",
            "gists_url": "https://api.github.com/users/rashad2985/gists{/gist_id}",
            "gravatar_id": "",
            "html_url": "https://github.com/rashad2985",
            "id": 5437974,
            "login": "rashad2985",
            "organizations_url": "https://api.github.com/users/rashad2985/orgs",
            "received_events_url": "https://api.github.com/users/rashad2985/received_events",
            "repos_url": "https://api.github.com/users/rashad2985/repos",
            "site_admin": false,
            "starred_url": "https://api.github.com/users/rashad2985/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/rashad2985/subscriptions",
            "type": "User",
            "url": "https://api.github.com/users/rashad2985"
        },
        "comments_url": "https://api.github.com/repos/rashad2985/ba_planar_branch/commits/31faae8f82b43c3220fd280caa79b567ff03aa3c/comments",
        "commit": {
            "author": {
                "date": "2016-11-19T09:46:23Z",
                "email": "rashad2985@gmail.com",
                "name": "Rashad Asgarbayli"
            },
            "comment_count": 0,
            "committer": {
                "date": "2016-11-19T09:46:23Z",
                "email": "rashad2985@gmail.com",
                "name": "Rashad Asgarbayli"
            },
            "message": "clean up in master.",
            "tree": {
                "sha": "d70101782593240723250c14a50b753f9ac779bf",
                "url": "https://api.github.com/repos/rashad2985/ba_planar_branch/git/trees/d70101782593240723250c14a50b753f9ac779bf"
            },
            "url": "https://api.github.com/repos/rashad2985/ba_planar_branch/git/commits/31faae8f82b43c3220fd280caa79b567ff03aa3c"
        },
        "committer": {
            "avatar_url": "https://avatars.githubusercontent.com/u/5437974?v=3",
            "events_url": "https://api.github.com/users/rashad2985/events{/privacy}",
            "followers_url": "https://api.github.com/users/rashad2985/followers",
            "following_url": "https://api.github.com/users/rashad2985/following{/other_user}",
            "gists_url": "https://api.github.com/users/rashad2985/gists{/gist_id}",
            "gravatar_id": "",
            "html_url": "https://github.com/rashad2985",
            "id": 5437974,
            "login": "rashad2985",
            "organizations_url": "https://api.github.com/users/rashad2985/orgs",
            "received_events_url": "https://api.github.com/users/rashad2985/received_events",
            "repos_url": "https://api.github.com/users/rashad2985/repos",
            "site_admin": false,
            "starred_url": "https://api.github.com/users/rashad2985/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/rashad2985/subscriptions",
            "type": "User",
            "url": "https://api.github.com/users/rashad2985"
        },
        "html_url": "https://github.com/rashad2985/ba_planar_branch/commit/31faae8f82b43c3220fd280caa79b567ff03aa3c",
        "parents": [
            {
                "html_url": "https://github.com/rashad2985/ba_planar_branch/commit/7e672a3b1992bb10af72e435dd225dd7e771468d",
                "sha": "7e672a3b1992bb10af72e435dd225dd7e771468d",
                "url": "https://api.github.com/repos/rashad2985/ba_planar_branch/commits/7e672a3b1992bb10af72e435dd225dd7e771468d"
            }
        ],
        "sha": "31faae8f82b43c3220fd280caa79b567ff03aa3c",
        "url": "https://api.github.com/repos/rashad2985/ba_planar_branch/commits/31faae8f82b43c3220fd280caa79b567ff03aa3c"
    },
    {
        "author": {
            "avatar_url": "https://avatars.githubusercontent.com/u/5437974?v=3",
            "events_url": "https://api.github.com/users/rashad2985/events{/privacy}",
            "followers_url": "https://api.github.com/users/rashad2985/followers",
            "following_url": "https://api.github.com/users/rashad2985/following{/other_user}",
            "gists_url": "https://api.github.com/users/rashad2985/gists{/gist_id}",
            "gravatar_id": "",
            "html_url": "https://github.com/rashad2985",
            "id": 5437974,
            "login": "rashad2985",
            "organizations_url": "https://api.github.com/users/rashad2985/orgs",
            "received_events_url": "https://api.github.com/users/rashad2985/received_events",
            "repos_url": "https://api.github.com/users/rashad2985/repos",
            "site_admin": false,
            "starred_url": "https://api.github.com/users/rashad2985/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/rashad2985/subscriptions",
            "type": "User",
            "url": "https://api.github.com/users/rashad2985"
        },
        "comments_url": "https://api.github.com/repos/rashad2985/ba_planar_branch/commits/7e672a3b1992bb10af72e435dd225dd7e771468d/comments",
        "commit": {
            "author": {
                "date": "2016-11-19T09:44:18Z",
                "email": "rashad2985@gmail.com",
                "name": "Rashad Asgarbayli"
            },
            "comment_count": 0,
            "committer": {
                "date": "2016-11-19T09:44:18Z",
                "email": "rashad2985@gmail.com",
                "name": "Rashad Asgarbayli"
            },
            "message": "merged.",
            "tree": {
                "sha": "ed1738748432109a5a5157e9bcb0ebac30eed4cc",
                "url": "https://api.github.com/repos/rashad2985/ba_planar_branch/git/trees/ed1738748432109a5a5157e9bcb0ebac30eed4cc"
            },
            "url": "https://api.github.com/repos/rashad2985/ba_planar_branch/git/commits/7e672a3b1992bb10af72e435dd225dd7e771468d"
        },
        "committer": {
            "avatar_url": "https://avatars.githubusercontent.com/u/5437974?v=3",
            "events_url": "https://api.github.com/users/rashad2985/events{/privacy}",
            "followers_url": "https://api.github.com/users/rashad2985/followers",
            "following_url": "https://api.github.com/users/rashad2985/following{/other_user}",
            "gists_url": "https://api.github.com/users/rashad2985/gists{/gist_id}",
            "gravatar_id": "",
            "html_url": "https://github.com/rashad2985",
            "id": 5437974,
            "login": "rashad2985",
            "organizations_url": "https://api.github.com/users/rashad2985/orgs",
            "received_events_url": "https://api.github.com/users/rashad2985/received_events",
            "repos_url": "https://api.github.com/users/rashad2985/repos",
            "site_admin": false,
            "starred_url": "https://api.github.com/users/rashad2985/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/rashad2985/subscriptions",
            "type": "User",
            "url": "https://api.github.com/users/rashad2985"
        },
        "html_url": "https://github.com/rashad2985/ba_planar_branch/commit/7e672a3b1992bb10af72e435dd225dd7e771468d",
        "parents": [
            {
                "html_url": "https://github.com/rashad2985/ba_planar_branch/commit/044167b8a69d4d50b8a185f01d76c0c27759b6a6",
                "sha": "044167b8a69d4d50b8a185f01d76c0c27759b6a6",
                "url": "https://api.github.com/repos/rashad2985/ba_planar_branch/commits/044167b8a69d4d50b8a185f01d76c0c27759b6a6"
            },
            {
                "html_url": "https://github.com/rashad2985/ba_planar_branch/commit/e467660dfb3cbfc729b8041d8798f807a5bc785f",
                "sha": "e467660dfb3cbfc729b8041d8798f807a5bc785f",
                "url": "https://api.github.com/repos/rashad2985/ba_planar_branch/commits/e467660dfb3cbfc729b8041d8798f807a5bc785f"
            }
        ],
        "sha": "7e672a3b1992bb10af72e435dd225dd7e771468d",
        "url": "https://api.github.com/repos/rashad2985/ba_planar_branch/commits/7e672a3b1992bb10af72e435dd225dd7e771468d"
    },
    {
        "author": {
            "avatar_url": "https://avatars.githubusercontent.com/u/5437974?v=3",
            "events_url": "https://api.github.com/users/rashad2985/events{/privacy}",
            "followers_url": "https://api.github.com/users/rashad2985/followers",
            "following_url": "https://api.github.com/users/rashad2985/following{/other_user}",
            "gists_url": "https://api.github.com/users/rashad2985/gists{/gist_id}",
            "gravatar_id": "",
            "html_url": "https://github.com/rashad2985",
            "id": 5437974,
            "login": "rashad2985",
            "organizations_url": "https://api.github.com/users/rashad2985/orgs",
            "received_events_url": "https://api.github.com/users/rashad2985/received_events",
            "repos_url": "https://api.github.com/users/rashad2985/repos",
            "site_admin": false,
            "starred_url": "https://api.github.com/users/rashad2985/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/rashad2985/subscriptions",
            "type": "User",
            "url": "https://api.github.com/users/rashad2985"
        },
        "comments_url": "https://api.github.com/repos/rashad2985/ba_planar_branch/commits/044167b8a69d4d50b8a185f01d76c0c27759b6a6/comments",
        "commit": {
            "author": {
                "date": "2016-11-19T09:43:19Z",
                "email": "rashad2985@gmail.com",
                "name": "Rashad Asgarbayli"
            },
            "comment_count": 0,
            "committer": {
                "date": "2016-11-19T09:43:19Z",
                "email": "rashad2985@gmail.com",
                "name": "Rashad Asgarbayli"
            },
            "message": "edit in master",
            "tree": {
                "sha": "c48f2faa56b69e80fd84829dbcc061738012775c",
                "url": "https://api.github.com/repos/rashad2985/ba_planar_branch/git/trees/c48f2faa56b69e80fd84829dbcc061738012775c"
            },
            "url": "https://api.github.com/repos/rashad2985/ba_planar_branch/git/commits/044167b8a69d4d50b8a185f01d76c0c27759b6a6"
        },
        "committer": {
            "avatar_url": "https://avatars.githubusercontent.com/u/5437974?v=3",
            "events_url": "https://api.github.com/users/rashad2985/events{/privacy}",
            "followers_url": "https://api.github.com/users/rashad2985/followers",
            "following_url": "https://api.github.com/users/rashad2985/following{/other_user}",
            "gists_url": "https://api.github.com/users/rashad2985/gists{/gist_id}",
            "gravatar_id": "",
            "html_url": "https://github.com/rashad2985",
            "id": 5437974,
            "login": "rashad2985",
            "organizations_url": "https://api.github.com/users/rashad2985/orgs",
            "received_events_url": "https://api.github.com/users/rashad2985/received_events",
            "repos_url": "https://api.github.com/users/rashad2985/repos",
            "site_admin": false,
            "starred_url": "https://api.github.com/users/rashad2985/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/rashad2985/subscriptions",
            "type": "User",
            "url": "https://api.github.com/users/rashad2985"
        },
        "html_url": "https://github.com/rashad2985/ba_planar_branch/commit/044167b8a69d4d50b8a185f01d76c0c27759b6a6",
        "parents": [
            {
                "html_url": "https://github.com/rashad2985/ba_planar_branch/commit/e294c7021c965f2ce813bb55749963e60bd08e29",
                "sha": "e294c7021c965f2ce813bb55749963e60bd08e29",
                "url": "https://api.github.com/repos/rashad2985/ba_planar_branch/commits/e294c7021c965f2ce813bb55749963e60bd08e29"
            }
        ],
        "sha": "044167b8a69d4d50b8a185f01d76c0c27759b6a6",
        "url": "https://api.github.com/repos/rashad2985/ba_planar_branch/commits/044167b8a69d4d50b8a185f01d76c0c27759b6a6"
    },
    {
        "author": {
            "avatar_url": "https://avatars.githubusercontent.com/u/5437974?v=3",
            "events_url": "https://api.github.com/users/rashad2985/events{/privacy}",
            "followers_url": "https://api.github.com/users/rashad2985/followers",
            "following_url": "https://api.github.com/users/rashad2985/following{/other_user}",
            "gists_url": "https://api.github.com/users/rashad2985/gists{/gist_id}",
            "gravatar_id": "",
            "html_url": "https://github.com/rashad2985",
            "id": 5437974,
            "login": "rashad2985",
            "organizations_url": "https://api.github.com/users/rashad2985/orgs",
            "received_events_url": "https://api.github.com/users/rashad2985/received_events",
            "repos_url": "https://api.github.com/users/rashad2985/repos",
            "site_admin": false,
            "starred_url": "https://api.github.com/users/rashad2985/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/rashad2985/subscriptions",
            "type": "User",
            "url": "https://api.github.com/users/rashad2985"
        },
        "comments_url": "https://api.github.com/repos/rashad2985/ba_planar_branch/commits/e467660dfb3cbfc729b8041d8798f807a5bc785f/comments",
        "commit": {
            "author": {
                "date": "2016-11-19T09:41:45Z",
                "email": "rashad2985@gmail.com",
                "name": "Rashad Asgarbayli"
            },
            "comment_count": 0,
            "committer": {
                "date": "2016-11-19T09:41:45Z",
                "email": "rashad2985@gmail.com",
                "name": "Rashad Asgarbayli"
            },
            "message": "edit in branch",
            "tree": {
                "sha": "1569966785715806196750169570f114e2a03285",
                "url": "https://api.github.com/repos/rashad2985/ba_planar_branch/git/trees/1569966785715806196750169570f114e2a03285"
            },
            "url": "https://api.github.com/repos/rashad2985/ba_planar_branch/git/commits/e467660dfb3cbfc729b8041d8798f807a5bc785f"
        },
        "committer": {
            "avatar_url": "https://avatars.githubusercontent.com/u/5437974?v=3",
            "events_url": "https://api.github.com/users/rashad2985/events{/privacy}",
            "followers_url": "https://api.github.com/users/rashad2985/followers",
            "following_url": "https://api.github.com/users/rashad2985/following{/other_user}",
            "gists_url": "https://api.github.com/users/rashad2985/gists{/gist_id}",
            "gravatar_id": "",
            "html_url": "https://github.com/rashad2985",
            "id": 5437974,
            "login": "rashad2985",
            "organizations_url": "https://api.github.com/users/rashad2985/orgs",
            "received_events_url": "https://api.github.com/users/rashad2985/received_events",
            "repos_url": "https://api.github.com/users/rashad2985/repos",
            "site_admin": false,
            "starred_url": "https://api.github.com/users/rashad2985/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/rashad2985/subscriptions",
            "type": "User",
            "url": "https://api.github.com/users/rashad2985"
        },
        "html_url": "https://github.com/rashad2985/ba_planar_branch/commit/e467660dfb3cbfc729b8041d8798f807a5bc785f",
        "parents": [
            {
                "html_url": "https://github.com/rashad2985/ba_planar_branch/commit/e294c7021c965f2ce813bb55749963e60bd08e29",
                "sha": "e294c7021c965f2ce813bb55749963e60bd08e29",
                "url": "https://api.github.com/repos/rashad2985/ba_planar_branch/commits/e294c7021c965f2ce813bb55749963e60bd08e29"
            }
        ],
        "sha": "e467660dfb3cbfc729b8041d8798f807a5bc785f",
        "url": "https://api.github.com/repos/rashad2985/ba_planar_branch/commits/e467660dfb3cbfc729b8041d8798f807a5bc785f"
    },
    {
        "author": {
            "avatar_url": "https://avatars.githubusercontent.com/u/5437974?v=3",
            "events_url": "https://api.github.com/users/rashad2985/events{/privacy}",
            "followers_url": "https://api.github.com/users/rashad2985/followers",
            "following_url": "https://api.github.com/users/rashad2985/following{/other_user}",
            "gists_url": "https://api.github.com/users/rashad2985/gists{/gist_id}",
            "gravatar_id": "",
            "html_url": "https://github.com/rashad2985",
            "id": 5437974,
            "login": "rashad2985",
            "organizations_url": "https://api.github.com/users/rashad2985/orgs",
            "received_events_url": "https://api.github.com/users/rashad2985/received_events",
            "repos_url": "https://api.github.com/users/rashad2985/repos",
            "site_admin": false,
            "starred_url": "https://api.github.com/users/rashad2985/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/rashad2985/subscriptions",
            "type": "User",
            "url": "https://api.github.com/users/rashad2985"
        },
        "comments_url": "https://api.github.com/repos/rashad2985/ba_planar_branch/commits/e294c7021c965f2ce813bb55749963e60bd08e29/comments",
        "commit": {
            "author": {
                "date": "2016-11-19T09:38:24Z",
                "email": "rashad2985@gmail.com",
                "name": "Rashad Asgarbayli"
            },
            "comment_count": 0,
            "committer": {
                "date": "2016-11-19T09:38:24Z",
                "email": "rashad2985@gmail.com",
                "name": "Rashad Asgarbayli"
            },
            "message": "blablabla",
            "tree": {
                "sha": "926d2c2e16318a38337370cbdb13e4410e3c8a08",
                "url": "https://api.github.com/repos/rashad2985/ba_planar_branch/git/trees/926d2c2e16318a38337370cbdb13e4410e3c8a08"
            },
            "url": "https://api.github.com/repos/rashad2985/ba_planar_branch/git/commits/e294c7021c965f2ce813bb55749963e60bd08e29"
        },
        "committer": {
            "avatar_url": "https://avatars.githubusercontent.com/u/5437974?v=3",
            "events_url": "https://api.github.com/users/rashad2985/events{/privacy}",
            "followers_url": "https://api.github.com/users/rashad2985/followers",
            "following_url": "https://api.github.com/users/rashad2985/following{/other_user}",
            "gists_url": "https://api.github.com/users/rashad2985/gists{/gist_id}",
            "gravatar_id": "",
            "html_url": "https://github.com/rashad2985",
            "id": 5437974,
            "login": "rashad2985",
            "organizations_url": "https://api.github.com/users/rashad2985/orgs",
            "received_events_url": "https://api.github.com/users/rashad2985/received_events",
            "repos_url": "https://api.github.com/users/rashad2985/repos",
            "site_admin": false,
            "starred_url": "https://api.github.com/users/rashad2985/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/rashad2985/subscriptions",
            "type": "User",
            "url": "https://api.github.com/users/rashad2985"
        },
        "html_url": "https://github.com/rashad2985/ba_planar_branch/commit/e294c7021c965f2ce813bb55749963e60bd08e29",
        "parents": [
        ],
        "sha": "e294c7021c965f2ce813bb55749963e60bd08e29",
        "url": "https://api.github.com/repos/rashad2985/ba_planar_branch/commits/e294c7021c965f2ce813bb55749963e60bd08e29"
    },
    {
        "author": {
            "avatar_url": "https://avatars.githubusercontent.com/u/5437974?v=3",
            "events_url": "https://api.github.com/users/rashad2985/events{/privacy}",
            "followers_url": "https://api.github.com/users/rashad2985/followers",
            "following_url": "https://api.github.com/users/rashad2985/following{/other_user}",
            "gists_url": "https://api.github.com/users/rashad2985/gists{/gist_id}",
            "gravatar_id": "",
            "html_url": "https://github.com/rashad2985",
            "id": 5437974,
            "login": "rashad2985",
            "organizations_url": "https://api.github.com/users/rashad2985/orgs",
            "received_events_url": "https://api.github.com/users/rashad2985/received_events",
            "repos_url": "https://api.github.com/users/rashad2985/repos",
            "site_admin": false,
            "starred_url": "https://api.github.com/users/rashad2985/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/rashad2985/subscriptions",
            "type": "User",
            "url": "https://api.github.com/users/rashad2985"
        },
        "comments_url": "https://api.github.com/repos/rashad2985/ba_planar_branch/commits/309de25cd5cd09da5abb57b7e36050ca01af5f7a/comments",
        "commit": {
            "author": {
                "date": "2016-11-19T09:45:24Z",
                "email": "rashad2985@gmail.com",
                "name": "Rashad Asgarbayli"
            },
            "comment_count": 0,
            "committer": {
                "date": "2016-11-19T09:45:24Z",
                "email": "rashad2985@gmail.com",
                "name": "Rashad Asgarbayli"
            },
            "message": "new edit in branch",
            "tree": {
                "sha": "39865d90d9f1a758dcc14547777ebe19d0f27d12",
                "url": "https://api.github.com/repos/rashad2985/ba_planar_branch/git/trees/39865d90d9f1a758dcc14547777ebe19d0f27d12"
            },
            "url": "https://api.github.com/repos/rashad2985/ba_planar_branch/git/commits/309de25cd5cd09da5abb57b7e36050ca01af5f7a"
        },
        "committer": {
            "avatar_url": "https://avatars.githubusercontent.com/u/5437974?v=3",
            "events_url": "https://api.github.com/users/rashad2985/events{/privacy}",
            "followers_url": "https://api.github.com/users/rashad2985/followers",
            "following_url": "https://api.github.com/users/rashad2985/following{/other_user}",
            "gists_url": "https://api.github.com/users/rashad2985/gists{/gist_id}",
            "gravatar_id": "",
            "html_url": "https://github.com/rashad2985",
            "id": 5437974,
            "login": "rashad2985",
            "organizations_url": "https://api.github.com/users/rashad2985/orgs",
            "received_events_url": "https://api.github.com/users/rashad2985/received_events",
            "repos_url": "https://api.github.com/users/rashad2985/repos",
            "site_admin": false,
            "starred_url": "https://api.github.com/users/rashad2985/starred{/owner}{/repo}",
            "subscriptions_url": "https://api.github.com/users/rashad2985/subscriptions",
            "type": "User",
            "url": "https://api.github.com/users/rashad2985"
        },
        "html_url": "https://github.com/rashad2985/ba_planar_branch/commit/309de25cd5cd09da5abb57b7e36050ca01af5f7a",
        "parents": [
            {
                "html_url": "https://github.com/rashad2985/ba_planar_branch/commit/e467660dfb3cbfc729b8041d8798f807a5bc785f",
                "sha": "e467660dfb3cbfc729b8041d8798f807a5bc785f",
                "url": "https://api.github.com/repos/rashad2985/ba_planar_branch/commits/e467660dfb3cbfc729b8041d8798f807a5bc785f"
            }
        ],
        "sha": "309de25cd5cd09da5abb57b7e36050ca01af5f7a",
        "url": "https://api.github.com/repos/rashad2985/ba_planar_branch/commits/309de25cd5cd09da5abb57b7e36050ca01af5f7a"
    }
]
